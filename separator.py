'''
Created on Oct 4, 2018

@author: Saad
'''
import os
import sys

MY_HASHFILE_NAME = sys.argv[-1]

SCRIPT_DIR = os.path.abspath(os.path.join(__file__,'..'))
infile_path = os.path.join(SCRIPT_DIR,MY_HASHFILE_NAME)

if not os.path.exists(infile_path):
	raise Exception('Input hashes filepath does not exist. Filepath: '+infile_path)

hash_1_id = "$1$"
hash_2_id = "$argon2i$"
hash_3_id = "$6$"
hash_4_id = "$5$"
hash_5_id = "$pbkdf2-sha256$"

hash_1_filename = "hashfile_md5crypt.txt"
hash_2_filename = "hashfile_argon2i.txt"
hash_3_filename = "hashfile_sha512crypt.txt"
hash_4_filename = "hashfile_sha256crypt.txt"
hash_5_filename = "hashfile_pbkdf2.txt"
hash_6_filename = "hashfile_des.txt"

hash_1_filep = open(hash_1_filename,'w')
hash_2_filep = open(hash_2_filename,'w')
hash_3_filep = open(hash_3_filename,'w')
hash_4_filep = open(hash_4_filename,'w')
hash_5_filep = open(hash_5_filename,'w')
hash_6_filep = open(hash_6_filename,'w')

with open(infile_path,'r') as infilep:
    for line in infilep:
        if line.startswith(hash_1_id):
            outfilep = hash_1_filep
        elif line.startswith(hash_2_id):
            outfilep = hash_2_filep
        elif line.startswith(hash_3_id):
            outfilep = hash_3_filep
        elif line.startswith(hash_4_id):
            outfilep = hash_4_filep
        elif line.startswith(hash_5_id):
            outfilep = hash_5_filep
        else:
            outfilep = hash_6_filep
        line = line.strip()
        outfilep.write(line + '\n')

outfilep.close()
hash_1_filep.close()
hash_2_filep.close()
hash_3_filep.close()
hash_4_filep.close()
hash_5_filep.close()
hash_6_filep.close()
