'''
Created on Nov 7, 2018

@author: Geese Howard
'''
import re

dict_path = r'E:\Studies\Scalable_Computing\cs7ns1\assignments\practical5\TestProj\realhuman_phill.txt'
outfilepath = r'E:\Studies\Scalable_Computing\cs7ns1\assignments\practical5\TestProj\crackstation_less_than_5_letters_lowercase.txt'

i=0
with open(outfilepath,'w') as outfile:
    with open(dict_path,'r') as infile:
        while True:
            try:
                line = infile.readline().strip()
            except:
                continue
            if not line:
                i+=1
                if i==3:
                    break
                continue
            if len(line)>5:
                continue
            if re.match("^[A-Z]*$",line):
                outfile.write(line+'\n')
                i=0