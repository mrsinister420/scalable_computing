'''
Created on Nov 2, 2018

@author: Geese Howard
'''
#!/usr/bin/python
import os,json,simplejson
import secretsharing as sss
from hashlib import sha256
import base64
from Crypto.Cipher import AES
from Crypto import Random


SCRIPT_DIR = os.path.abspath(os.path.join(__file__,os.pardir))
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]

def pxor(pwd,share):
    '''
      XOR a hashed password into a Shamir-share

      1st few chars of share are index, then "-" then hexdigits
      we'll return the same index, then "-" then xor(hexdigits,sha256(pwd))
      we truncate the sha256(pwd) to if the hexdigits are shorter
      we left pad the sha256(pwd) with zeros if the hexdigits are longer
      we left pad the output with zeros to the full length we xor'd
    '''
    words=share.split("-")
    hexshare=words[1]
    slen=len(hexshare)
    hashpwd=sha256(pwd).hexdigest()
    hlen=len(hashpwd)
    outlen=0
    if slen<hlen:
        outlen=slen
        hashpwd=hashpwd[0:outlen]
    elif slen>hlen:
        outlen=slen
        hashpwd=hashpwd.zfill(outlen)
    else:
        outlen=hlen
    xorvalue=int(hexshare, 16) ^ int(hashpwd, 16) # convert to integers and xor 
    paddedresult='{:x}'.format(xorvalue)          # convert back to hex
    paddedresult=paddedresult.zfill(outlen)       # pad left
    result=words[0]+"-"+paddedresult              # put index back
    return result

def pwds_shares_to_secret(kpwds,kinds,diffs):
    '''
        take k passwords, indices of those, and the "public" shares and 
        recover shamir secret
    '''
    shares=[]
    for i in range(0,len(kpwds)):
        shares.append(pxor(kpwds[i],diffs[kinds[i]]))
    secret=sss.SecretSharer.recover_secret(shares)
    return secret

def encrypt(raw, key):
    raw = pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))

def decrypt(enc, password):
    enc = base64.b64decode(enc)
    iv = enc[:AES.block_size]
    cipher = AES.new(password, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[AES.block_size:]))

def get_all_cracked_hashes(cracked_dir):
    out = set([])
    if not os.path.exists(cracked_dir):
        raise Exception('Cracked dir does not exist')
    for x in os.walk(cracked_dir):
        for f in x[2]:
            filepath = os.path.join(x[0],f)
            filep = open(filepath,'r')
            lines = filep.readlines()
            for line in lines:
                if not ':' in line:
                    continue
                if line.startswith('sha256'):
                    line = pbdkdf2_deformat(line)
                out.add(line.strip())
    return sorted(list(out))

def pbdkdf2_format(h):
    ret = h.replace('$pbkdf2-sha256','sha256').replace('$29000$',':29000:')
    return ret

def pbdkdf2_deformat(h):
    h = h.replace('sha256:','$pbkdf2-sha256$')
    for _ in range(2):
        col_index = h.find(':')
        h = h[:col_index]+'$'+h[col_index+1:]
    return h

def main():
    for layer_ind in range(1,9):
        layer_dir = r'E:\Studies\Scalable_Computing\cs7ns1\assignments\practical5\TestProj\layer'+str(layer_ind)
        outdir = layer_dir
        layer_number = layer_dir[-1]
        nextlayer_path = os.path.join(outdir,'layer'+str(int(layer_number)+1)+'.json')
        layer_secret_filepath = os.path.join(outdir,'layer_secret.txt')
        json_path = os.path.join(layer_dir,os.path.basename(layer_dir)+'.json')
        cracked_hashes_filepath =os.path.join(layer_dir,'cracked')
    
        cracked_hashes = get_all_cracked_hashes(cracked_hashes_filepath)
    
        with open(json_path) as f:
            mydict = json.load(f)
    
        shares = [str(x) for x in mydict['shares']]
    
        cpwds = []
        cpwds_inds = []
    
        for ind,h in enumerate(mydict['hashes']):
            h_tmp = h+':'
            for c_hash in cracked_hashes:
                if h_tmp in c_hash:
                    cpwds.append(str(c_hash.replace(h_tmp,'')))
                    cpwds_inds.append(ind)
        
        secret = pwds_shares_to_secret(cpwds,cpwds_inds,shares)
        decrypted = decrypt(mydict['ciphertext'], secret.zfill(32).decode('hex'))
    
    
    
        print('========================================')
        if decrypted[0]=='{' and decrypted[-1]=='}':
            print('Level cracked!')
        else:
            print('Level not cracked.')
            return
        print('LAYER: '+layer_number)
        print("SECRET: "+secret)
        print("SEGMENT OF LAYER: "+decrypted[4:50])
        with open(layer_secret_filepath, 'w') as outfile:
            outfile.write(secret)
        with open(nextlayer_path, 'w') as outfile:
            outfile.write(simplejson.dumps(simplejson.loads(decrypted), indent=4))
        print('========================================')

if __name__=='__main__':
    main()