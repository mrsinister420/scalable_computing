'''
Created on Oct 11, 2018

@author: Saad
'''
import os

MY_HASHFILE_NAME = 'maliksa.hashes'
SCRIPT_DIR = os.path.abspath(os.path.join(__file__,'..'))
CRACKED_FILES_DIR = os.path.join(SCRIPT_DIR,'cracked')
MY_HASHFILE_PATH = os.path.join(SCRIPT_DIR,MY_HASHFILE_NAME)
CRACKED_HASHES_FILENAME = 'my_cracked_hashes'
CRACKED_HASHFILE_PATH = os.path.join(SCRIPT_DIR,CRACKED_HASHES_FILENAME)
SUBMISSION_FILEPATH = os.path.join(SCRIPT_DIR,'submission.broken')


def read_lines(filepath):
    filep = open(filepath,'r')
    lines = filep.readlines()
    filep.close()
    lines_stripped = [line.strip() for line in lines]
    return lines_stripped

def get_all_cracked_hashes(cracked_dir):
    out = set([])
    if not os.path.exists(cracked_dir):
        raise Exception('Cracked dir does not exist')
    for x in os.walk(cracked_dir):
        for f in x[2]:
            filepath = os.path.join(x[0],f)
            filep = open(filepath,'r')
            lines = filep.readlines()
            for line in lines:
                if not ':' in line:
                    continue
                if line.startswith('sha256'):
                    line = pbdkdf2_deformat(line)
                out.add(line.strip())
    return sorted(list(out))

def save_to_file(in_list,filename):
    outfile_path = os.path.join(SCRIPT_DIR,filename)
    outfilep = open(outfile_path,'w')
    for line in in_list:
        outfilep.write(line+"\n")
    outfilep.close()

def pbdkdf2_format(h):
    ret = h.replace('$pbkdf2-sha256','sha256').replace('$29000$',':29000:')
    return ret

def pbdkdf2_deformat(h):
    last_col_index = h.rfind(':')
    h = h[:last_col_index].replace('sha256','$pbkdf2-sha256')+h[last_col_index:]
    h = h[:last_col_index].replace(':','$')+h[last_col_index:]
    return h

def get_uncracked_hashes(cracked_list):
    cracked_hashes = set([x.split(':')[0] for x in cracked_list])
    all_list = set(read_lines(MY_HASHFILE_PATH))
    uncracked_hashes = all_list.difference(cracked_hashes)
    return sorted(list(uncracked_hashes))

def generate_submission():
    outlines = []
    outfilep = open(SUBMISSION_FILEPATH,'w')
    in_lines = read_lines(CRACKED_HASHFILE_PATH)
    for line in in_lines:
        if not line.startswith('$'):
            if not line.startswith('sha256'):
                tokens = line.split(':')
                val = tokens[-1]
                if len(val)>7:
                    val = val[:7]
                    line = tokens[0]+':'+val
            else:
                last_col_index = line.rfind(':')
                line = line[:last_col_index].replace('sha256','$pbkdf2-sha256')+line[last_col_index:]
                line = line[:last_col_index].replace(':','$')+line[last_col_index:]
        line = line.replace(':',' ')
        line = line+'\n'
        outlines.append(line)
    outfilep.writelines(outlines)
    outfilep.close()

cracked_list = get_all_cracked_hashes(CRACKED_FILES_DIR)
save_to_file(cracked_list,'my_cracked_hashes')
uncracked_list = get_uncracked_hashes(cracked_list)
save_to_file(uncracked_list,'uncracked_hashes')
generate_submission()